# DCryptor #

DCryptor is a windows PE encoding tool that I made for a computer security course.  It's very basic and was done as a learning exercise, but it was a very fun project!

### What does it do? ###

* DCryptor takes the path to a Windows PE binary (.exe, .dll) and encodes it.
* It encodes the .text and .data sections and drops a stub in the last section of the file, expanding that last section and flagging it as executable
* For simplicity, it disables the address randomization bit of the PE Optional Header
* It changes the entry point to a stub which decodes the .text and .data section then jumps back to the original entry point
* It uses a simple XOR operation with an optionally turning key for encoding.  It's not a good solution for any real-world application!