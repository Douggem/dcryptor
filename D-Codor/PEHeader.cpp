#include "stdafx.h"
#include "PEHeader.h"
namespace PEHeader{

	// Gets the address of the NTSignature word/DWORD given the DOS header
	// This can represent two different things depending on what the signature actually is
	void* GetNTSignature(IMAGE_DOS_HEADER* header)
	{
		void* ntSignatureAddress = ADD_POINTER(header, header->e_lfanew);
		return ntSignatureAddress;
	}

	// Pre-NT file headers start with the NTSignature as the first 2 bytes	
	IMAGE_FILE_HEADER* GetPEH(IMAGE_DOS_HEADER* header) {
		return (IMAGE_FILE_HEADER*)GetNTSignature(header);
	}

	// The NT file header starts 4 bytes after the NTSignature
	IMAGE_FILE_HEADER* GetPEHNT(IMAGE_DOS_HEADER* header) 
	{
		return (IMAGE_FILE_HEADER*)ADD_POINTER(GetNTSignature(header), sizeof(DWORD));
	}

	// Gets the optional PENT header which isn't optional at all and has really important things in it
	IMAGE_OPTIONAL_HEADER* GetPEOHNT(IMAGE_DOS_HEADER* header) 
	{
		return (IMAGE_OPTIONAL_HEADER*)ADD_POINTER(GetPEHNT(header), sizeof(IMAGE_FILE_HEADER));
	}
	
	DWORD GetImageFileType(IMAGE_DOS_HEADER* header)
	{
		USHORT fileType = header->e_magic;
		if (fileType == IMAGE_DOS_SIGNATURE)	// Double check that it's a valid DOS/Windows executable.  Just in case.
		{
			// NT_SIGNATURE is 4 bytes, the others are 2 bytes
			DWORD ntSignature = *(DWORD*)GetNTSignature(header);
			WORD ntSignatureLo = (WORD)ntSignature;
			if (ntSignature = IMAGE_NT_SIGNATURE)
				return ntSignature;
			else if (ntSignatureLo == IMAGE_OS2_SIGNATURE || ntSignatureLo == IMAGE_OS2_SIGNATURE_LE || ntSignatureLo == IMAGE_DOS_SIGNATURE)
				return ntSignatureLo;
		}
		else
		{
			// Wow you really ran this on a non dos executable?  
			return NULL;
		}
	}

	// Gets the first Section Header, which is also a pointer to the first byte
	// in the section header array.
	IMAGE_SECTION_HEADER* GetFirstSectionHeader(IMAGE_DOS_HEADER* header)
	{
		IMAGE_SECTION_HEADER* firstSectionHeader = (IMAGE_SECTION_HEADER*)ADD_POINTER(GetPEOHNT(header), sizeof(IMAGE_OPTIONAL_HEADER));
		return firstSectionHeader;
	}

	// Returns the number of section headers in a DOS header
	int GetNumSections(IMAGE_DOS_HEADER* header)
	{
		IMAGE_FILE_HEADER* fileHeader = GetPEHNT(header);
		if (fileHeader) {
			return fileHeader->NumberOfSections;
		}
		return 0;
	}
}

