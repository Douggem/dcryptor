// D-Codor.cpp 
// Douglas "Douggem" Confere 2015
// Made for a computer security course because it seemed like a fun project
//

#include "stdafx.h"
#include "PEHeader.h"
#include "D-Coder.h"
using namespace DUtils;


// One massive main method?  Check!
int _tmain(int argc, _TCHAR* argv[])
{
	char buffer[256];
	HMODULE ourModule = GetModuleHandle(nullptr);
	IMAGE_DOS_HEADER* header = (IMAGE_DOS_HEADER*)ourModule;
	
	wcstombs(buffer, argv[0], sizeof(buffer));
	printf("Encoding executable file %s\n", buffer);
	DCryptor* crypt = new DCryptor(buffer, "\x11\x22\x33\x44\x55");
	crypt->Encode();	

	// Remove the file extension
	char* ext = nullptr;
	char* temp = buffer;	
	while ((temp = strchr(temp, '.')), temp != nullptr) {
		ext = temp++;
	}
	*ext = '\0';
	strcat(buffer, "_enc.exe");
	crypt->WriteToDisk(buffer);
	printf("Saved encoded file to %s\n", buffer);
	getchar(); 
	return 0;
}

